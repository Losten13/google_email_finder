### Email Finder Application

# Connect to AWS

1. Download the .pem file from keypair.
2. In Amazon Dashboard choose "Instances" from the left side bar, and then select the instance you would like to connect to.
3, Click on "Actions", then select "Connect"
4. Click on "Connect with a Standalone SSH Client"
(INSTRUCTIONS FOR MAC)
5. Open up a Terminal window
6. Create a directory:
`mkdir -p ~/.ssh`
7. Move the downloaded connection.pem file to the .ssh directory we just created:
`mv ~/Downloads/connection.pem ~/.ssh`
8. Change the permissions of the .pem file so only the root user can read it:
`chmod 400 ~/.ssh/connection.pem`
9. Create a config file:
`vim ~/.ssh/config`
10. Enter the following text into that config file:
```
Host *amazonaws.com
IdentityFile ~/.ssh/connection.pem
User ec2-user
```
11. Save that file pressing ESC :wq ENTER

11. Use the ssh command with your public DNS hostname to connect to the instance (above the current DNS, but check the Public DNS column to use the right one)
`ssh ec2-13-58-133-90.us-east-2.compute.amazonaws.com`

# Installing Python 3
1. Make sure the environment is updated
`sudo yum update -y`
2. Install python 3.6
`sudo yum install python36`
3. Fix Pip file - get it
`curl -O https://bootstrap.pypa.io/get-pip.py`
4. Fix Pip file - set it up for python3
`python3 get-pip.py —-user`


# Setup directions

1. Create a virtual environment as follows:
    
    Use python 3.6.4 <br />
	python3 -m pip virtualenv

2. Activate virtual environment created above:

	source venv/bin/activate

3. Switch to project folder and install dependencies:

	pip3 install -r requirements.txt

This will install all dependencies required.

# Running the application:

Start the application with the command:

	python3 email_finder_refactored_beta.py
	
If there is chrome error, restart machine then type:

    Xvfb :99 -screen 0 1024x768x16 &

Run continuously: <br />
    tmux ls <br />
    tmux <br />
    detach the terminal using Ctrl + b and then press d <br />
    tmux attach <br />
There are some flags you can use while running the script to remove previously output files as follows:

1. clear_previous_stats: Clear previous excel file having stats (File in filesPath['excel'])

2. clear_previous_email_list: Clear previous email List (File in filesPath['output'])

3. clear_previous_research_kw: Clear previously researched keywords (File in filesPath['researched_keywords'])

4. clear_all_previous_outputs: Clear all 3 files from above.
