# from googleapiclient.discovery import build
import pprint
import time
from googlesearch import search

# Doc
# https://google-api-client-libraries.appspot.com/documentation/customsearch/v1/python/latest/customsearch_v1.cse.html
# Create cse id https://cse.google.com

url_list = []

def APIkeys():
    my_api_key = "AIzaSyCrptVAkcYtihjEvarIfUQ1D9Y8m7dWEwc"
    my_cse_id = "009760256560591582871:rhta4ps8j2c"
    my_api_key_zcm = "AIzaSyBLmqvn_x-JD9C06yxZIqnCmhaUs-tRi5o"
    my_cse_id_zcm = "016727820626831266116:li6do-e9l94"

    return my_api_key, my_cse_id, my_api_key_zcm, my_cse_id_zcm

def google_search(item, start, api_key = APIkeys()[0], cse_id =APIkeys()[1]):
    """
        Info on custom search API
        https://google-api-client-libraries.appspot.com/documentation/customsearch/v1/python/latest/customsearch_v1.cse.html
    """
    service = build("customsearch", "v1",
            developerKey=api_key)
    res = service.cse().list(q=item, cx=cse_id, googlehost = 'google.com.au', start=start).execute()
    if not 'items' in res:
        print('No result !!\nres is: {}'.format(res))
    else:
        for item in res['items']:
            # print('=================================================')
            # # print(item['displayLink'])
            # print(item['link'])
            # urls_list.append(item['link'])
            url_list.append(item)
    new_list = url_list.copy()
    return new_list

# def multi_google_search(item, pnum):
#     for i in range(pnum):
#         results = google_search(item=item,start=pnum*10+1)
#     return results

urls = []
def multi_google_search(item):
    for url in search(item, start= 0, num=50, stop=50):
        urls.append(url)
    return urls


def getLinks(searchTerms, advoidSites = [], num = 10, start = 0, stop = 10, pause = 2, tld = "com"):
    urls = []
    top3 = {}

    # Load URL results
    for item in searchTerms:
        searchResults = []
        for j in search(item, tld = tld, num = num, start = start, stop = stop, pause = pause, only_standard = True): 
            searchResults.append(j)

        top3[item] = searchResults[:3]

        # Exclude URLs which are in advoid files
        if advoidSites: 
            searchResults = [url for url in searchResults if not any(url for advoid in advoidSites if advoid in url)]
        
        # Add to output file, including search term, as a tuple
        urls.extend([(url, item) for url in searchResults])
        
    return urls, top3