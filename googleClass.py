import os
# os.environ['http_proxy']=''

from time import sleep
from googlesearch import search
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.chrome.options import Options
import selenium.common.exceptions as selExcep 
from googleAPI import multi_google_search
from avoid_sites import remove_unwanted_websites
import tldextract
import os
import sys
from bs4 import BeautifulSoup  # HTML Parser
from urllib.parse import quote_plus
from PIL import Image
import re

class GoogleSearch():
	def __init__(self, driver_path = "", user_profile = ""):
		self.folder = os.path.dirname(os.path.abspath(sys.argv[0]))
		self.printsFolder = os.path.join(self.folder, "Prints")
		self.createFolder(self.printsFolder)

		CHROMEDRIVER_PATH = driver_path if driver_path else os.path.join(self.folder, "chromedriver")
		# WINDOW_SIZE = "1920,1080"
		WINDOW_SIZE = "800,480"

		self.chrome_options = Options()  
		self.chrome_options.add_argument("--window-size=%s" % WINDOW_SIZE)
		self.chrome_options.add_argument("--enable-screenshot-testing-with-mode")
		self.chrome_options.add_argument("--enable-usermedia-screen-capturing")
		self.chrome_options.add_argument("--allow-http-screen-capture")
		self.chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
		# self.chrome_options.add_extension(self.getExtension("keywords"))

		if user_profile:
			self.chrome_options.add_argument("user-data-dir=%s" % user_profile)
		
		prefs = {
                'download.default_directory': self.folder,
                'download.prompt_for_download': False,
                'download.directory_upgrade': True,
                'safebrowsing.enabled': False,
                'safebrowsing.disable_download_protection': True,
                "profile.default_content_setting_values.notifications" : 2}

		self.chrome_options.add_experimental_option("prefs", prefs)

		self.driver = webdriver.Chrome(executable_path = CHROMEDRIVER_PATH,
					                   chrome_options = self.chrome_options
					                  )
		# self.driver
		print("DRIVER STARTED.......")

	def get(self, url, wait_id = ""):
		self.driver.get(url)
		if wait_id:
			WebDriverWait(self.driver, 1000).until(EC.presence_of_element_located((By.ID, wait_id)))


	def quit(self):
		if hasattr(self, 'driver'):
			self.driver.quit()

	@staticmethod
	def createFolder(path):
		try:
			os.mkdir(path)
			print("   - Folder \"{}\" created".format(path))
		except FileExistsError: pass

	def getExtension(self, nameToSearch):
		files = os.listdir(self.folder)
		extensionFile = [file for file in files if ".crx" in file and nameToSearch in file.lower()]
		return extensionFile[0] if extensionFile else extensionFile

	def setKeywordsEverywhere(self, apiKey):
		self.get("chrome-extension://hbapdpeemoojbophdfndmlgdhppljgmp/html/options.html", wait_id = "apiKey")
		
		apiInput = self.driver.find_element_by_id("apiKey")
		apiInput.clear()
		apiInput.send_keys(apiKey)

		validateButton = self.driver.find_element_by_id("validate")
		validateButton.click()

	def getKeywordsPrintsAndVolumes(self, queryTerms):
		keywordsVolumes = {}
		print("queryTerms")
		print(queryTerms)
		for term in queryTerms:
			t = term[2]
			keywordsVolumes[term] = self.getPrintAndVolume(t)
			self.adjustPrint(t)
			
		return keywordsVolumes
	
	def webSiteName(self, base_url):
		url_info = tldextract.extract(base_url)
		domain_name = url_info.domain
		return domain_name

	def getPrintAndVolume(self, term, page_num=1):
		encoded = quote_plus(term)
		self.get("https://www.google.com/search?q={}&start={}".format(encoded, page_num*10))#, wait_id = "xt-info"

		# get print file
		filePath = os.path.join(self.printsFolder, "{}.png".format(term))
		self.driver.save_screenshot(filePath)
		print("self.driver.save_screenshot(filePath)")
		# get search numbers

		searchVolumeField = self.driver.find_element_by_id("xt-info")
		searchVolume = searchVolumeField.text

		print(vars(searchVolumeField))
		print(searchVolume)

		return self.getSeparatedSearchVolumeStats(searchVolume)

	@staticmethod
	def getSeparatedSearchVolumeStats(searchVolume):
		pattern = "(?<={}: )[$0-9,.]*"
		volume = re.search(pattern.format("Volume"), searchVolume).group(0)
		cpc = re.search(pattern.format("CPC"), searchVolume).group(0)
		competition = re.search(pattern.format("Competition"), searchVolume).group(0)
		return {"volume": volume, "cpc": cpc, "competition": competition}

	def adjustPrint(self, term):
		originalFilePath = os.path.join(self.printsFolder, "{}.png".format(term))
		newFilePath = os.path.join(self.printsFolder, "{}-data.png".format(term))

		# Crop original image
		image_obj = Image.open(originalFilePath)
		red_box = Image.open(os.path.join(self.printsFolder, "_DONT_EDIT_red_box.png"))

		# Mixin with red_box image and save with new name
		# Crop co-ordinates will be different on different locations, since MacOS, Windows and Chrome all have different visuals and browsers can have various toolbars and trims on top.
		coords = (0, 0, 1600, 185)
		# (Previous statement: default willbe MacOS coordinates.)
		print('Operating System is: ' + os.name)
		if os.name == 'posix':
			print('Platform is: ' + sys.platform)
			if sys.platform == 'linux':
				# For Ubuntu/CentOS
				coords = (0, 0, 800, 100)
				print('Original Size: ', red_box.size)
				red_box.thumbnail((int(red_box.size[0]/2), int(red_box.size[1]/2)), Image.ANTIALIAS)
				print('After resizing: ', red_box.size)
			elif sys.platform == 'darwin':
				# For MacOS
				coords = (0, 0, 1600, 185)
		elif os.name == 'nt':
			# copied macOS now. Don't have windows machine.
			coords = (0, 0, 1600, 185)
		cropped_image = image_obj.crop(coords)

		cropped_image.paste(red_box, mask = red_box)
		cropped_image.save(newFilePath)

		# Delete original file
		os.remove(originalFilePath)

	@staticmethod
	def getLinks(searchTerms, advoidSites = [], num = 50, start = 0, stop = 50, pause = 2, tld = "com"):
		urls = []
		# top3_placeholder = {}
		top3 = {}

		print(
			'start: ', start, '\n'
			'num: ', num, '\n',
			'stop: ', stop, '\n',
			'pause: ', pause, '\n',
			)

		# print(searchTerms)

		# Load URL results
		for item in searchTerms:
			searchResults = []
			for result in multi_google_search(item=item):
				searchResults.append(result['link'])

			# while len(top3) < 3:
			# 	top3[item] = searchResults[:3]

			# Exclude URLs which are in advoid files
			# if advoidSites: 
			# 	searchResults = [url for url in searchResults if not any(url for advoid in advoidSites if advoid in url)]

			searchResults1 = remove_unwanted_websites(searchResults)
			# Check if the first three emails have duplicates 
			count_removal = 0

			while True:
				removed = 0
				for i in searchResults1[:3]:
					first_three = tldextract.extract(i).domain
					for j in searchResults1[11:51]:
						later = tldextract.extract(j).domain
						if later == first_three:
							searchResults1.remove(i)
							removed += 1
							count_removal += removed
				if removed == 0:
					break


			# Get the top 3 urls 
			
			top3[item] = searchResults1[:3]

			# for i in top3[item]:
			# 	url_info = tldextract.extract(base_url)
				
			# Delete the first 10 urls 
			del searchResults1[:11-count_removal]

			# Add to output file, including search term, as a tuple
			urls.extend([(url, item) for url in searchResults1])
		    
		return urls, top3
