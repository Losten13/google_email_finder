# Retrieve emails and company names from a Google search 

import csv
import json  # Load query
import os  # Manipulate file location inside the system
import re  # Filter
import sys
import traceback
from math import log  # For Website name spliter
from urllib.parse import urlparse  # Get Website base URL

import linkGrabber
import openpyxl
import tldextract  # Get Website name
import urllib3
import xlrd
import xlsxwriter
from bs4 import BeautifulSoup  # HTML Parser
from find_job_titles import FinderAcora  # Find Job Titles

from gSearch import googleSearchFix
from googleClass import GoogleSearch

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import argparse

from openpyxl import Workbook, load_workbook

from googleAPI import multi_google_search

from connect import connection

# if you want use countries which not include use two letters format
# from countries.txt
TARGET_COUNTRY = input("Type 'AUS' or 'US' or 'CA' or 'UK' or 'NZ': ")  # Python 3

# Regular expressions for email validation. One time compilation while starting script. Re compilation is costly.
regex_user = re.compile(
    r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*\Z"  # dot-atom
    r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*"\Z)',  # quoted-string
    re.IGNORECASE)
regex_domain = re.compile(
    # max length for domain name labels is 63 characters per RFC 1034
    r'((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+)(?:[A-Z0-9-]{2,63}(?<!-))\Z',
    re.IGNORECASE)


def get_files_path():
    # folderPath = os.path.abspath(os.path.join(""))
    folderPath = os.path.dirname(os.path.abspath(sys.argv[0]))
    configFilePath = os.path.join(folderPath, "config.json")
    searchFilePath = os.path.join(folderPath, "Search.json")
    keyword_input = os.path.join(folderPath, f"keywords_{TARGET_COUNTRY}.xlsx")
    excelPath = os.path.join(folderPath, f'emails_{TARGET_COUNTRY}.xlsx')
    websitesToAdvoidPath = os.path.join(folderPath, 'websites_to_advoid.txt')
    emailTitlesPath = os.path.join(folderPath, 'emails_titles-{}.xlsx')
    researched_keywords_op = os.path.join(folderPath, 'researched_keywords.csv')
    wordsByFrequencyPath = os.path.join(folderPath, 'words-by-frequency.txt')
    sshPath = os.path.join(folderPath, 'cert.pem')
    return {
        "root": folderPath,
        "config": configFilePath,
        "search": searchFilePath,
        "keyword_input": keyword_input,
        "advoid": websitesToAdvoidPath,
        "output": emailTitlesPath,
        "excel": excelPath,
        'researched_keywords': researched_keywords_op,
        "words": wordsByFrequencyPath,
        "ssh": sshPath
    }


def country_to_google_country_code(country):
    result = 'country'
    if (country == 'AUS'):
        return result + 'AU'
    elif (country == 'UK'):
        return result + 'GB'
    return result + country[0:2]


def get_links_from_google(searchTerms, advoidSites=[], num=50, start=0, stop=50):
    """
    start = first site to start gathering
    num = number of pages per search
    stop = last site to finish gathering
    """
    urls = []

    # Load URL results
    for item in searchTerms:
        for result in multi_google_search(item, 5):
            urls.append(result['link'])

    # Exclude URLs which are in advoid files
    return urls if not advoidSites else [url for url in urls if not any(url for advoid in advoidSites if advoid in url)]


def web_site_name(base_url):
    url_info = tldextract.extract(base_url)
    domain_name = url_info.domain
    return domain_name


def get_valid_links_from_url(url, base_url, domain_names, preferredKeywords=[]):
    # Get all links
    links = get_all_links(url)

    # Add http protocol if link doesn't contain it
    links = [add_protocol(link, base_url) for link in links]

    # Clean links, taking out the ones from files / emails / internal link / telephone,
    # duplicates and the ones who are not from the same domain
    excludeTerms = [".pdf", ".jpg", ".png", '.ca', '.uk', ".jpeg", "mailto", "#", "tel",
                    'gov', 'org', 'email', 'support', 'career', 'noreply', 'yoursite',
                    'example', 'pdf']
    links = [link.strip() + "/" if "/" not in link[-1] else link.strip() for link in links
             if not any(excludeTerm for excludeTerm in excludeTerms if excludeTerm in link)
             and tldextract.extract(link).domain == domain_name
             ]

    # Filter to check if a preferred Keyword is in the links
    # TODO
    # filteredLinks = filterForPreferredKeywords(links, preferredKeywords)
    filteredLinks = ""

    # return links if not link found with filtered value possible
    validLinks = links if not filteredLinks else filteredLinks

    return unique(validLinks)


def get_all_links(url):
    try:
        link_data = linkGrabber.Links(url)
        qq = link_data.find()
        return [link['href'] for link in qq]
    except Exception as e:
        print(e)
        print("linkGrabber not allowed for website. Exception found: ", e)
        return []


def filter_for_preferred_keywords(links):
    preferedKeywords = ["people", "professional", "team", "contact"]
    pattern = "(https?:\/\/)(.*\/).*{}"
    filteredLinks = [link for link in links for kw in preferedKeywords if re.match(pattern.format(kw), link)]
    return filteredLinks


def add_protocol(url, base_url):
    return url if 'http' in url else base_url[:-1] + url


def validate_domain(domain):
    """
        Validate domain part of the email address.
    """
    if regex_domain.match(domain):
        return True
    return False


def validate_email(value):
    """
        Validate detected email id as a legitimate string.
    """
    user, domain = value.rsplit('@', 1)
    if not regex_user.match(user):
        return False
    if validate_domain(domain):
        return True
    # If no domain not valid, try looking for possible IDN part.
    try:
        domain = domain.encode('idna').decode('ascii')
    except UnicodeError:
        # If no IDN part and domain invalid invalidate email.
        return False
    if validate_domain(domain):
        return True
    return False


def weed_out(emails):
    """
    Validate detected email address-like strings for correctness.
    Previous regexp can detect loosely if an email-address like string is present.
    Strings like: .timesTags@filterEmpty.17 showed up in a results from a query(Came from an inline javascript snippet).
    A string like: 26px-social@2x.png also showed up, but strings such as this one will likely be harder to call outliers.
    """

    # remove probable file extensions at the end
    def to_remove(address):
        exts = (
            '.png', 'png', '.jpg', '.jpeg', '.gif', '.pdf', 'pdf', '.doc', '.xls', '.xls', '.docx', '.gov', '.uk',
            '.ca',
            'gov', 'org', 'email', 'support', 'career', 'noreply', 'yoursite',
            'example', 'emailhost', 'yourdomain', 'yourname', 'someone',
            'wixpress', '.wixpress', 'wordpress', 'sentry',
            'domain', 'privacy', 'support', 'sentry', '.gov', '.io', 'io')
        for ext in exts:
            if address.lower().startswith(ext):
                return True
            elif address.lower().endswith(ext):
                return True
        if not validate_email(address):
            return True
        return False

    emails = [email for email in emails if (not to_remove(email))]
    return emails


def getUniqueMails(text):
    # mailtos = re.findall(r'[\w\.\+-]+@[\w-]+\.[\w\.-]+', text)
    mailtos = re.findall(r'[\w\.\+-]+@[\w-]+\.[a-z0-9_\.-]+[a-z0-9]', text)
    mailtos = [email.strip('.') if "http" not in email[-6] else email[:email.find("https")].strip('.') for email in
               mailtos]
    if mailtos != None:
        mailtos = weed_out(mailtos)
    else:
        pass
    print("mails found", mailtos)
    uniqueMailtos = unique(mailtos)
    return uniqueMailtos


def getTitles(text):
    finder = FinderAcora()
    potential_titles = finder.findall(text)
    uniquePotentialTitles = unique((title[2] for title in potential_titles))
    return uniquePotentialTitles if uniquePotentialTitles else "Job Title Not Found"


def WebsiteNameSplitter(s):
    # Infer the location of spaces in a string without spaces
    # Build a cost dictionary, assuming Zipf's law and cost = -math.log(probability).
    # wordsByFrequencyPath = os.path.join(, 'words-by-frequency.txt')
    words = open(filesPath["words"]).read().split()
    wordcost = dict((k, log((i + 1) * log(len(words)))) for i, k in enumerate(words))
    maxword = max(len(x) for x in words)

    # Find the best match for the i first characters, assuming cost has
    # been built for the i-1 first characters.
    # Returns a pair (match_cost, match_length).
    def best_match(i):
        candidates = enumerate(reversed(cost[max(0, i - maxword):i]))
        return min((c + wordcost.get(s[i - k - 1:i], 9e999), k + 1) for k, c in candidates)

    # Build the cost array.
    cost = [0]
    for i in range(1, len(s) + 1):
        c, k = best_match(i)
        cost.append(c)

    # Backtrack to recover the minimal-cost string.
    out = []
    i = len(s)
    while i > 0:
        c, k = best_match(i)
        assert c == cost[i]
        out.append(s[i - k:i])
        i -= k

    return " ".join(reversed(out))


def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]


def loadFile(filePath, toJson=False, lines=False):
    with open(filePath) as f:
        data = json.load(f) if toJson else f.read().splitlines() if lines else f.read()
    return data


def saveFile(filePath, source):
    header = ['WebsiteName', 'Email', 'Title', 'link']
    with open(filePath, 'w', encoding='utf-8') as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerow(header)
        writer.writerows(source)


def saveExcelFile(filePath, source, printsPath):
    print("\nCreating File {}".format(filePath))

    notAdded = True
    Added = False
    term = ""
    printImageStatus = {term: notAdded for term in list(set([line[4] for line in source]))}

    try:
        workbook = xlsxwriter.Workbook(filePath, {'strings_to_urls': False})
        worksheet = workbook.add_worksheet("Found Emails")
        worksheet.set_default_row(60)

        bold = workbook.add_format({'text_wrap': True, 'bold': 1})
        text_format = workbook.add_format({'text_wrap': True, "valign": "vcenter", "shrink": False})
        vertical_align = workbook.add_format({"valign": "vcenter"})

        print("   Success")
        headings = ['WebsiteName', 'Email', 'Title', 'link', "Keyword", "Website_url", "top3", "Searches", "CPC",
                    "Competition", "Screenshots"]
        worksheet.write_row('A1', headings, bold)

        # Adjust columns width and style
        worksheet.set_column(0, 0, 15, vertical_align)
        worksheet.set_column(1, 1, 25, vertical_align)
        worksheet.set_column(2, 2, 15, vertical_align)
        worksheet.set_column(3, 3, 20, vertical_align)
        worksheet.set_column(4, 4, 20, vertical_align)
        worksheet.set_column(5, 5, 20, vertical_align)
        worksheet.set_column(6, 6, 50, text_format)

        # Add content and images
        for i in range(len(source)):
            worksheet.write_row("A{}".format(i + 2), source[i], text_format)
            if printImageStatus[source[i][4]]:
                printPath = os.path.join(printsPath, "{}-data.png".format(source[i][4]))
                worksheet.insert_image("K{}".format(i + 2), printPath, {'x_scale': 0.5, 'y_scale': 0.5})
                printImageStatus[source[i][4]] = False

        workbook.close()
    except (xlsxwriter.exceptions.XlsxWriterException, Exception) as e:
        print("   File creation failed with following error: {}".format(e))


def get_already_researched_solutions(researched_solutions_file_name):
    """
    Industry mapped to a list of cities for which solutions have already been gathered.
    """
    already_researched = {}
    if not os.path.isfile(researched_solutions_file_name):
        return already_researched
    with open(researched_solutions_file_name, 'r') as solsFile:
        reader = csv.reader(solsFile)
        for row in reader:
            try:
                if row[0] in already_researched:
                    # If this industry has already been read once, append this city to the list of cities it has already read.
                    already_researched[row[0]].append(row[1])
                else:
                    # If this industry has not been seen before, add new to dictionary.
                    already_researched[row[0]] = [row[1]]
            except:
                print("Researched Solutions file is empty")
    return already_researched


def build_query_terms(input_file, researched_solutions_file_name):
    """
        Read query from xls file construct single search term.
    """
    res = list()
    wb = xlrd.open_workbook(input_file)
    industry_sheet = wb.sheet_by_index(0)
    region_sheet = wb.sheet_by_index(1)
    region_city_mappings = {}

    research_completed = get_already_researched_solutions(researched_solutions_file_name)

    for i in range(0, region_sheet.ncols):
        # Create a dictionary of regions mapped to a list of cities from this sheet.
        region_city_mappings[region_sheet.cell_value(0, i)] = [i for i in region_sheet.col_values(i, 1) if len(i) > 0]

    for i in range(1, industry_sheet.nrows):
        # ith row - marks profession/industry
        industry = industry_sheet.cell_value(i, 0)
        for j in range(1, industry_sheet.ncols):
            # jth column
            region = industry_sheet.cell_value(0, j)
            for city in region_city_mappings[region]:
                if industry not in research_completed or (
                        industry in research_completed and city not in research_completed[industry]):
                    # If research has not already been completed for this industry, continue forward, else don't
                    res.append((industry, city, industry + ' ' + city))
    return res


def save_intermediate_research_info(intermediate_output_file, industry, city):
    """
    Save intermediate research info on which industry and city has been researched.
    """
    if os.path.isfile(intermediate_output_file):
        with open(intermediate_output_file) as f:
            for row in csv.reader(f):
                # print(row)
                if row and (row[0] == industry and row[1] == city):
                    return
    print("\nAppending researched keywords to intermediate file: {}".format(intermediate_output_file))
    with open(intermediate_output_file, 'a') as appendFile:
        # write a single industry + city combination.
        # potentially, more different stats could be added here.
        writer = csv.writer(appendFile)
        writer.writerow([industry, city])


def apppendIntermediateResultsToFile(filePath, source, printsPath):
    """
    Append intermediate results to a file.
    """
    print("\nAppending result to intermediate file: {}".format(filePath))

    notAdded = True
    Added = False
    term = ""
    printImageStatus = {term: notAdded for term in list(set([line[4] for line in source]))}
    print(len(source))
    if not os.path.isfile(filePath):
        try:
            workbook = xlsxwriter.Workbook(filePath, {'strings_to_urls': False})
            worksheet = workbook.add_worksheet("Found Emails")
            worksheet.set_default_row(60)

            bold = workbook.add_format({'bold': 1})
            text_format = workbook.add_format({'text_wrap': True, "valign": "vcenter", "shrink": False})
            vertical_align = workbook.add_format({"valign": "vcenter"})

            print("   Success")
            headings = ['WebsiteName', 'Email', 'Title', 'link', "Keyword", "Website_url", "top3", "Searches", "CPC",
                        "Competition", "ScreenshotPath"]
            worksheet.write_row('A1', headings, bold)

            # Adjust columns width and style
            worksheet.set_column(0, 0, 15, vertical_align)
            worksheet.set_column(1, 1, 25, vertical_align)
            worksheet.set_column(2, 2, 15, vertical_align)
            worksheet.set_column(3, 3, 20, vertical_align)
            worksheet.set_column(4, 4, 20, vertical_align)
            worksheet.set_column(5, 5, 20, vertical_align)
            worksheet.set_column(6, 6, 50, text_format)

            # Add content and images
            for i in range(len(source)):
                printPath = os.path.join(printsPath, "{}-data.png".format(source[i][4]))
                print('Screenshot Path: ' + printPath)
                source[i].append(printPath)
                worksheet.write_row("A{}".format(i + 2), source[i])
            workbook.close()
        except (xlsxwriter.exceptions.XlsxWriterException, Exception) as e:
            print("   File creation failed with following error: {}".format(e))
    else:
        print(filePath)
        workbook = openpyxl.load_workbook(filename=filePath)
        worksheet = workbook.get_sheet_by_name('Found Emails')
        row = worksheet.max_row + 1
        for i in range(len(source)):
            printPath = os.path.join(printsPath, "{}-data.png".format(source[i][4]))
            print('Screenshot Path: ' + printPath)
            source[i].append(printPath)
            worksheet.append(source[i])

        workbook.save(filePath)


def append_intermediate_output(filePath, source):
    header = ['WebsiteName', 'Email', 'Title', 'link']
    # write to workbook
    if os.path.isfile(filePath):
        workbook = load_workbook(filename=filePath)
        worksheet = workbook[workbook.sheetnames[0]]
    else:
        workbook = Workbook()
        worksheet = workbook.create_sheet("emails", 0)
        worksheet.append(header)
    next_row = worksheet.max_row + 1
    for i in source:
        worksheet.append(i[:4])
    workbook.save(filePath)


def clear_previous_outputs(files_list):
    """
    Remove every file present in the file, if it exists on the filesystem.
    """
    for file in files_list:
        if os.path.isfile(file):
            os.remove(file)


def splited_lines_generator(lines, n):
    """
    generate splits of length n each.
    """
    for i in range(0, len(lines), n):
        yield lines[i: i + n]


blocked_links = []
delete_links = []
ssh = []
output = []

if __name__ == "__main__":

    # Get path for all the files that will be used
    filesPath = get_files_path()
    print(filesPath["output"])
    parser = argparse.ArgumentParser()
    parser.add_argument('-clear_all_previous_outputs', action='store_true', help='Clear All previous Output Files.')
    parser.add_argument('-clear_previous_stats', action='store_true', help='Clear previous Excel files having stats.')
    parser.add_argument('-clear_previous_email_list', action='store_true', help='Clear Previous Email List.')
    parser.add_argument('-clear_previous_research_kw', action='store_true',
                        help='Clear previously researched keywords.')
    args = parser.parse_args()
    if args.clear_all_previous_outputs:
        clear_previous_outputs([filesPath['output'].format(""), filesPath['excel'], filesPath['researched_keywords']])
    else:
        if args.clear_previous_research_kw:
            clear_previous_outputs([filesPath['researched_keywords']])
        if args.clear_previous_email_list:
            clear_previous_outputs([filesPath['output'].format("")])
        if args.clear_previous_stats:
            clear_previous_outputs([filesPath['excel']])

    # Get config file
    config = loadFile(filesPath["config"], toJson=True)


    def override_where():
        """ overrides certifi.core.where to return actual location of cacert.pem"""
        # change this to match the location of cacert.pem
        return filesPath["ssh"]


    # Set SSH path for requests
    # is the program compiled?
    if hasattr(sys, "frozen"):
        import certifi.core

        os.environ["REQUESTS_CA_BUNDLE"] = override_where()
        certifi.core.where = override_where

        # delay importing until after where() has been replaced
        import requests.utils
        import requests.adapters

        # replace these variables in case these modules were
        # imported before we replaced certifi.core.where
        requests.utils.DEFAULT_CA_BUNDLE_PATH = override_where()
        requests.adapters.DEFAULT_CA_BUNDLE_PATH = override_where()

    queryTerms = build_query_terms(filesPath['keyword_input'], filesPath['researched_keywords'])

    # Log with the terms to be searched

    for queryTerm in queryTerms:
        print("\n-> Running for query term: {}".format(queryTerm))
        termIndex = queryTerm[2].replace(" ", "_")
        query_term_output = []
        batch_output = []

        checkSearchVolume = True
        # Take print screen from terms in Google:
        try:
            print('Before google_search successful')
            googleSearch = GoogleSearch(user_profile="chrome_profile")
            print('fine before if')
            if config["take_print_screens_and_volumes"]:
                print('if is successful')
                print("\nGetting Search Volumes and prints from Google (from \"Keywords Everywhere\" extension)")
                print('API KEY: ', config["keywords_everywhere_API_key"])
                # googleSearch.setKeywordsEverywhere(apiKey = config["keywords_everywhere_API_key"])
                keywordsSearchVolume = googleSearch.getKeywordsPrintsAndVolumes([queryTerm])
                print("avoid file")
                print(filesPath["advoid"])
            else:
                print('if failed')
                print(
                    "\nDummy volumes created (if you want to have real data, change \"take_print_screens_and_volumes\" to 'true' on config.json")
                keywordsSearchVolume = {term: {"volume": 1, "cpc": 0, "competition": 0} for term in [queryTerm]}
            googleSearch.quit()
        except Exception as e:
            checkSearchVolume = False
            keywordsSearchVolume = {term: {"volume": 0, "cpc": 0, "competition": 0} for term in [queryTerm]}
            traceback.print_exc()
            # print(e.stacktrace())
            print("  Printscreens might not be taken due to the following exception(s):")
            print("   - {}".format(e))
            try:
                googleSearch.exit()
            except:
                pass

        # Remove search terms that have less than minimum searches
        if config["filter_queries_by_search_volume"] and checkSearchVolume:
            if int(keywordsSearchVolume[queryTerm]["volume"].replace(',', '')) <= config['min_search_threshold']:
                save_intermediate_research_info(filesPath['researched_keywords'], queryTerm[0], queryTerm[1])
                continue

        sitesToAdvoid = loadFile(filesPath["advoid"], lines=True)

        print('Sending to Top Level Domain: ' + config['googleSearch_tld'])

        GoogleURLs = []
        top3Results = {}

        # Load URL results
        for item in [queryTerm]:
            searchResults = []
            print(country_to_google_country_code(TARGET_COUNTRY))
            for j in googleSearchFix(item[2], tld=config["googleSearch_tld"], num=50, start=0, stop=50, pause=1,
                                     only_standard=True,
                                     extra_params={'cr': country_to_google_country_code(TARGET_COUNTRY)}):
                searchResults.append(j)

            # Exclude URLs which are in advoid files
            searchResults = [url for url in searchResults if not any(url for advoid in sitesToAdvoid if advoid in url)]
            print(searchResults)
            top3Results[item] = searchResults[:3]

            # Add to output file, including search term, as a tuple
            GoogleURLs.extend([(url, item) for url in searchResults])

        # Log of URLs to be opened and read by the crawler
        print("---------------- URLs TO BE READ ----------------")
        for n, (link, term) in enumerate(GoogleURLs):
            print(n, link)

        print("----------------------------")

        for url, term in GoogleURLs:
            print(term)
            print("Running URL... {}".format(url))
            parsed_url = urlparse(url)
            base_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_url)

            connected, _ = connection(url)
            if not connected:
                blocked_links.append(url)
                continue

            domain_name = web_site_name(url)  # Get Website name
            website_name = WebsiteNameSplitter(domain_name).title()

            ssh.append("ssh Secure" if parsed_url.scheme == "https" else "ssh NOT Secure")

            validLinks = get_valid_links_from_url(url, base_url, domain_name,
                                                  config["filter_for_preferred_keywords_on_url"])
            if not validLinks: continue

            # Log of links to be opened and checked for email contacts
            print("**** VALID LINKS TO BE SEARCHED ****")
            for n, link in enumerate(validLinks):
                print(n, link)

            count = 0

            for link in validLinks[:10]:
                print("\n- Opening link {}".format(link))
                try:
                    connected, htmlContent = connection(link, successMsg="Searching...", failMsg="404/Not allowed")
                except UnicodeError as ue:
                    print('Skipping link: ', link, ' due to malformed url.')
                    continue

                # Check if returned any html to be parsed by BeautifulSoup
                if not htmlContent:
                    continue

                try:
                    soup = BeautifulSoup(htmlContent, features="html.parser")
                    text = soup.prettify(formatter=None)
                    uniqueMailtos = getUniqueMails(text)
                except:
                    continue

                # Jump to the next loop in case there are no UniqueMails or they are already in output
                newEmails = [email.lower() for email in uniqueMailtos if email.lower() not in output]
                if not uniqueMailtos or not newEmails or (output and not any(newEmails)):
                    print("No new emails found")
                    count += 1
                    continue

                # Only save x number of emails per website
                if len(newEmails) < 2:
                    pass
                elif len(newEmails) == 2:
                    break
                else:
                    newEmails[:2]
                    break

                print("{} new Email(s) found to be appended:".format(len(newEmails)))
                print(newEmails)
                jobTitles = getTitles(text)
                print("Job titles found:", jobTitles)

                for email in newEmails:
                    parsedUrl = urlparse(link)
                    baseUrl = '{uri.scheme}://{uri.netloc}/'.format(uri=parsedUrl)
                    print(term)
                    if email and email.strip():
                        emailInfoToSave = [
                            website_name,
                            email,
                            "\n".join(jobTitles),
                            link,
                            term[2],
                            baseUrl,
                            "\n".join(top3Results[term]),
                            keywordsSearchVolume[term]["volume"],
                            keywordsSearchVolume[term]["cpc"],
                            keywordsSearchVolume[term]["competition"]
                        ]
                        batch_output.append(emailInfoToSave)
                        output.append(email)

        # Add remaning outputs which were not matched in a batch to the output
        query_term_output.extend(batch_output)
        print('\n--------------------------------------------------------')
        print('Finished searches for: ', term)
        print('And {} emails were found'.format(len(query_term_output)))
        print('--------------------------------------------------------\n')

        # Save in the middle, when all links for a site have been visited and checked.for a specific query
        apppendIntermediateResultsToFile(filesPath["excel"], query_term_output, googleSearch.printsFolder)
        # save intermediate info that specific target has been researched.
        save_intermediate_research_info(filesPath['researched_keywords'], term[0], term[1])
        # add remaining emails to output file for this term.
        # append_intermediate_output(filesPath['output'].format(termIndex), batch_output)

# For bulk save:
# saveFile(filesPath["output"], output)
# saveExcelFile(filesPath["excel"], output, googleSearch.printsFolder)
