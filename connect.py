import urllib.request  # Open a connection
# from proxy_utils import _get_proxies, random_proxy # proxy_utils.py to get proxies.
# from fake_useragent import UserAgent # Add a layer of fake user-agent for websites that block bot's access
import requests
import time

# proxies = _get_proxies()
# proxy_index = random_proxy(proxies)
# print('No. of proxies fetched: ', len(proxies), 'Random proxy index chosen: ', proxy_index)
proxies = []
proxy_index = []
def connection(url, successMsg = "Connection allowed\n", failMsg = "No connection allowed\n", headers={}, use_proxy=False):
	# declare proxies as a global variable of name proxies that will be accessed from above.
	global proxies
	# headers = {"User-Agent": UserAgent().chrome, "Referer": "https://www.google.com/search"}
	connected = False
	output = ""
	n_tries = 0
	# global proxy_index
	# proxy = proxies[proxy_index]
	while(n_tries < 1 and not connected):
		# Backoff from trying after 5 attempts and connection failure.
		try:
			# if use_proxy:
			# 	# Use proxy as stated by user.
			# 	# result = requests.get(url, verify = False if (os.name == "nt") else filesPath["ssh"], headers = headers, timeout = 10)
			# 	print("Using proxy with index", proxy_index)
			# 	prox_con = {
			# 		'http': 'http://'+proxy['ip']+ ':' + proxy['port'],
			# 		'https': 'http://'+proxy['ip']+ ':' + proxy['port']
			# 	}
			# 	print('Using Proxy: ', proxy)
			# 	result = requests.get(url, verify = False, headers = headers, timeout = 10, proxies=prox_con)
			# else:
			# without proxy
			# print('Connecting without proxy')
			result = requests.get(url, verify = False, headers = headers, timeout = 10)
			connected = result.status_code == requests.codes.ok
			output = result.text
		except requests.exceptions.RequestException as e:
			failMsg += " - Error found during requests: {}".format(e)
			connected = False
			output = failMsg
			printStatus = {False: failMsg, True: successMsg}
		finally:
			printStatus = {False: failMsg, True: successMsg}
			print('Connection status: ', connected, 'Message: ', printStatus[connected])
			print('Cannot connect to proxy' in printStatus[connected])
			# if not connected:
			# 	if printStatus[connected].startswith('404')
			# if (not printStatus[connected].startswith('404') and not connected) or (printStatus[connected].startswith('404') and not connected):
			if (not connected):
				# if failure message starts with a 404, no changes need to be made to the proxy.
				# if failure message starts does not start with a 404, and the connection failed, proxy will be changed.
				# if not printStatus[connected].startswith('404') or (printStatus[connected].startswith('404') and 'Cannot connect to proxy' in printStatus[connected]):

				# Contrary to above comment, its better to use the metric that:
				# 1. all 404 responses will lead to new proxy being used for all future requests, since for a scraper to backoff, a company might decide to misguide the scraper into a 404.
				# 2. A 403 was also found in some cases to be working when hit with a different frequency. Most likely, a 403 will happen when a certain page will need login credentials from its website.
				# if printStatus[connected].startswith('404') or printStatus[connected].startswith('403') or 'Cannot connect to proxy' in printStatus[connected] or 'Max retries exceeded with url' in printStatus[connected] or 'ConnectTimeoutError' in printStatus[connected] or 'No connection allowed' in printStatus[connected]:
				# Stale logic. Replace later.
				n_tries += 1
				if use_proxy:
					print('Proxy ' + proxy['ip'] + ':' + proxy['port'] + ' deleted.')
					del proxies[proxy_index]
					if len(proxies) == 0:
						proxies = _get_proxies()
					proxy_index = random_proxy(proxies)
					proxy = proxies[proxy_index]
				else:
					# Some sort of logging must be done to save the information that it is not connected.
					pass
			else:
				break
		time.sleep(2)
	return connected, output
