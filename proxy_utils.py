'''
Author: Divij Sehgal(divij.sehgaal7@gmail.com)
'''

from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import random

ua = UserAgent() # From here we generate a random user agent
proxies = [] # Will contain proxies [ip, port]


def _get_proxies(proxy=None):
	"""
	Retrieve latest proxies and scrape them from the page.
	"""
	# Get Page with Proxy data.
	proxies_req = Request('https://www.sslproxies.org/')
	proxies_req.add_header('User-Agent', ua.random)
	found_proxies = False
	while not found_proxies:
		try:
			proxies_doc = urlopen(proxies_req).read().decode('utf8')
			found_proxies = True
			break
		except Exception as ex:
			print('Error fetching proxies: ' + ex)
			pass

	# Scrape from page using bs
	soup = BeautifulSoup(proxies_doc, 'html.parser')
	proxies_table = soup.find(id='proxylisttable')

	# Save proxies in the array
	if len(proxies) == 0:
		res = proxies_table.tbody.find_all('tr')
		print(len(res))
		for row in proxies_table.tbody.find_all('tr'):
			if 'elite' in row.find_all('td')[4].get_text() or 'anonymous' in row.find_all('td')[4].get_text():
				proxies.append({
				  'ip':   row.find_all('td')[0].string,
				  'port': row.find_all('td')[1].string
				})
			# break
			# sys.exit(0)
		print('fetched ' , len(proxies) , ' proxies')

	return proxies

# Main function
def main():
	pass
	# Retrieve latest proxies
	proxies_req = Request('https://www.sslproxies.org/')
	proxies_req.add_header('User-Agent', ua.random)
	proxies_doc = urlopen(proxies_req).read().decode('utf8')

	soup = BeautifulSoup(proxies_doc, 'html.parser')
	proxies_table = soup.find(id='proxylisttable')

	# Save proxies in the array
	for row in proxies_table.tbody.find_all('tr'):
		proxies.append({
		  'ip':   row.find_all('td')[0].string,
		  'port': row.find_all('td')[1].string
		})

	# Choose a random proxy
	proxy_index = random_proxy()
	proxy = proxies[proxy_index]

	for n in range(1, 100):
		req = Request('http://icanhazip.com')
		req.set_proxy(proxy['ip'] + ':' + proxy['port'], 'http')

		# Every 10 requests, generate a new proxy
		if n % 10 == 0:
			proxy_index = random_proxy()
			proxy = proxies[proxy_index]

		# Make the call
		try:
			my_ip = urlopen(req).read().decode('utf8')
			print('#' + str(n) + ': ' + my_ip)
		except: # If error, delete this proxy and find another one
			del proxies[proxy_index]
			print('Proxy ' + proxy['ip'] + ':' + proxy['port'] + ' deleted.')
			proxy_index = random_proxy()
			proxy = proxies[proxy_index]

# Retrieve a random index proxy (we need the index to delete it if not working)
def random_proxy(local_proxies=None):
	"""
	Two cases here:
	1. If target list of proxies is supplied while calling the function, generate random index in its range.
	2. Else(When local_proxies is a None Object), use script-local proxies object defined above.
	"""
	try:
		if local_proxies is not None:
			return random.randint(0, len(local_proxies) - 1)
		else:
			return random.randint(0, len(proxies) - 1)
	except ValueError as ex:
		return -1

if __name__ == '__main__':
	main()
