import os
import tldextract


path = os.getcwd() + '/websites_to_advoid.txt'

# Load websites to advoid
f = open(path)
website_names = [line.rstrip('\n') for line in f] 

url_domain_name = {}

def webSiteName(base_url):
    """
        Input: takes one url string 
        Output: domain name of the input url
    """
    url_info = tldextract.extract(base_url)
    domain_name = url_info.domain
    return domain_name

def remove_unwanted_websites(websites_to_check):
    """
        Input: list of website urls
        Output: cleaned list of website urls
    """
    # Add website url and domain name to a dictionary
    for i in websites_to_check:
        url_domain_name[i] = webSiteName(i)
    
    # Remove unwanted websites  
    for i in url_domain_name:
        counter = set(website_names) & set(url_domain_name[i])
        if counter != None:
            websites_to_check.remove(i)

    return websites_to_check

# For testing, uncomment the following
# websites_to_check = ['www.seek.com', 'www.jora.com']
# remove_unwanted_websites(websites_to_check)

