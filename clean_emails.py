import pandas as pd
import re
import os
from collections import namedtuple

global input
path = os.getcwd() + '/email_finder2/websites_to_advoid.txt'

input = pd.read_excel('/Users/zcm/Desktop/email/email_sender/raw_email_files/AUS_71.xlsx')

def delete_avoid_websites(input):
    dict = {}
    avoids = []
    with open(path) as f:
        for line in f:
            avoids.append(line.rstrip())
    emails = input.iloc[:,1]
    websites = input.iloc[:,0]
    
    for website,email in zip(websites,emails):
        if website not in dict:
            dict[website] = 1
        if dict[website] < 4:
            domains = email.split("@")[1].split(".")
            name = domains[0]
            if name in avoids:
                input = input[input.Email != email]
            elif name[0].isdigit():
                input = input[input.Email != email]
            elif 'gov' in domains:
                input = input[input.Email != email]
            else:
                dict[website] = dict[website] + 1
        else:
            input = input[input.Email != email]
    return input

def delete_unwanted_emails(input):
    """
        Delete emails with a domain in exts list
    """
    emails = input.iloc[:,1]

    exts = ['.png', '.jpg', '.jpeg', '.gif', '.doc', '.xls', '.xls', '.docx',
             'org', 'email', 'support', 'career', 'noreply', 'yoursite',
                                'example', 'emailhost','yourdomain', 'yourname','someone',
                                'wixpress','wordpress', 'uk', 'ca','de','sentry',
                                'domain','privacy', 'name','fairfaxmedia', 'companyname',
                                'productreview', 'mysite', 'mail']

    for line in exts:
        for email in emails:
            domains = email.split("@")[1].split(".")
            name = domains[0]
            if line == name:
                input = input[input.Email != email]

    return input


new_input = delete_avoid_websites(input)
final_input = delete_unwanted_emails(new_input)
final_input.to_excel('cleaned_AUS_71.xlsx')